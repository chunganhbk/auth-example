package main

import (
	"fmt"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"gitlab.com/chunganhbk/auth-example/config"
	"gitlab.com/chunganhbk/auth-example/database"
	"gitlab.com/chunganhbk/auth-example/handlers"
	"log"
	"net/http"
)

func main(){
	// CONFIG
	err := config.Load("config/config.yaml")
	if err != nil {
		fmt.Println("Error: Failed to load configuration")
	}
	// DATABASE
	//fmt.Println("Opening database at...", config.State.DatabasePath)
	_, err = database.Storage.Open(config.State.DatabasePath)
	if err != nil {
		fmt.Println("Error: Failed to load database")
	}
	defer database.Storage.Close()
	r := chi.NewRouter();
	handlers.InitHandlers(r);
	// HTTP Server
	if !config.State.Debug {
		r.Use(middleware.Logger)
	}
	log.Println("The server is running on port", config.State.Port)
	log.Fatal(http.ListenAndServe(":"+ config.State.Port, r))

	http.ListenAndServe(":"+ config.State.Port, r)
}
