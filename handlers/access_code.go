package handlers

import (
	"github.com/go-chi/chi"
	"gitlab.com/chunganhbk/auth-example/helpers"
	"gitlab.com/chunganhbk/auth-example/models"
	"net/http"
)

type (
	AccessCodeHandler struct {
		handlerHelper
	}
)
func (h AccessCodeHandler) index(w http.ResponseWriter, r *http.Request)  {
	access,_ := (models.AccessCode{}).GetAll()
	user,_ := getUserSession(w,r)
	tmplHelper.Render(w,"admin_access",Map{"user": user,"access": access})
}
func (h AccessCodeHandler) getList(w http.ResponseWriter, r *http.Request)  {
	filter := models.UserFilter{
		Sort: models.Sort{
			SortKey: r.FormValue("SortKey"),
			SortVal: helpers.ToInt(r.FormValue("SortVal")),
		},
	}
	access,_ := (models.AccessCode{}).GetList(filter)
	h.JSON(w, access, http.StatusOK)
}
func (h AccessCodeHandler) update(w http.ResponseWriter, r *http.Request){
	access := models.AccessCode{
		ID: helpers.ToInt(r.FormValue("ID")),
		AccessCode: r.FormValue("AccessCode"),
	}
	if err := access.Update();err == nil {
		h.JSON(w,access.ID,http.StatusOK)
	}else{
		h.JSON(w,Map{"error":err.Error()},http.StatusBadRequest)
	}
}
func (h AccessCodeHandler) delete(w http.ResponseWriter, r *http.Request) {
	access := models.AccessCode{};
	access.ID = helpers.ToInt(chi.URLParam(r, "id"));
	if err := access.Delete();err == nil {
		h.JSON(w,access.ID,http.StatusOK)
	}else{
		h.JSON(w,Map{"error":err.Error()},http.StatusBadRequest)
	}
}
func (h AccessCodeHandler) getLogs(w http.ResponseWriter, r *http.Request) {

	logs,_ := (models.AccessLog{}).GetLogs(helpers.ToInt(chi.URLParam(r, "id")));
	//fmt.Printf("%+v\n", logs)
	user,_ := getUserSession(w,r)
	tmplHelper.Render(w,"admin_access_logs",Map{"user": user, "logs": logs})
}
func (h AccessCodeHandler) create(w http.ResponseWriter, r *http.Request)  {
	accessCode := models.AccessCode{
		AccessCode: r.FormValue("access_code"),
	}
	if err := accessCode.Create();err == nil {
		h.JSON(w,accessCode.ID,http.StatusOK)
	}else{
		h.JSON(w,Map{"error":err.Error()},http.StatusBadRequest)
	}

}