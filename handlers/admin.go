package handlers

import (
	"github.com/go-chi/chi"
	"net/http"
)
func adminRoute(r chi.Router)  {

	r.Use(needLoginMiddleware,adminMiddleware)
	/*access code*/
	accessHandler := AccessCodeHandler{}
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		user,err := getUserSession(w,r)
		if err != nil {
			http.Redirect(w, r, "/auth/login", 302)
			return
		}
		tmplHelper.Render(w,"admin_access",Map{"user":user})
	})
	r.Get("/access",accessHandler.index)
	r.Get("/access/list",accessHandler.getList)
	r.Get("/access/{id:[0-9]+}",accessHandler.getLogs)
	r.Post("/access/create",accessHandler.create)
	r.Put("/access/update",accessHandler.update)
	r.Delete("/access/{id:[0-9]+}",accessHandler.delete)
}

func adminMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		user,err := getUserSession(w,r)
		if err != nil && !user.Admin {
			http.Redirect(w, r, "/", 302)
			return
		}
		next.ServeHTTP(w, r)
	})
}