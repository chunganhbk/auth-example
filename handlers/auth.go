package handlers

import (
	"github.com/go-chi/chi"
	"gitlab.com/chunganhbk/auth-example/models"
	"net/http"
)

type (
	authHandler struct {
		handlerHelper
	}
)

func authRoute(r chi.Router) {
	h := authHandler{}
	r.Get("/login",h.loginPage)
	r.Post("/login",h.loginPost)
	r.Post("/login/confirm",h.confirmHandler)
	r.Get("/logout",h.logoutHandler)
	r.Get("/login-access-code", h.loginAccessCode);
	r.Post("/login-access-code", h.loginAccessCodePost);
	r.Post("/check-access-code", h.checkAccessCode);
}

func (h authHandler) loginPage(w http.ResponseWriter, r *http.Request)  {
	tmplHelper.Render(w,"auth_login",nil)
}

func (h authHandler) loginPost(w http.ResponseWriter, r *http.Request)  {
	user := models.User{
		UserName: r.FormValue("UserName"),
	}
	if err := user.GetByName();err == nil {
		if err := user.ComparePassword(r.FormValue("Password"));err == nil {
			loginUserSession(w, r, user)
			h.JSON(w,Map{"status":1},http.StatusOK)
		}else{
			h.JSON(w,Map{"error":"The user already exists but the password is not match"},http.StatusBadRequest)
		}
	}else{
		user.Password = r.FormValue("Password")
		store := sessionManager.Start(w, r)
		store.Set("cacheUser",user)
		h.JSON(w,Map{"status":0},http.StatusOK)
		//
		//if err := user.Create();err == nil {
		//	loginUserSession(w,r,user)
		//	h.JSON(w,Map{"userId":user.ID},http.StatusOK)
		//}else{
		//	h.JSON(w,Map{"error":err.Error()},http.StatusOK)
		//}
	}
}
func (h authHandler) loginAccessCode(w http.ResponseWriter, r *http.Request)  {
	tmplHelper.Render(w,"auth_login_access",nil)
}
func (h authHandler) checkAccessCode(w http.ResponseWriter, r *http.Request)  {
	accessCode := models.AccessCode{
		AccessCode: r.FormValue("access_code"),
	};
	if err := accessCode.GetByCode();err == nil {
		store := sessionManager.Start(w, r);
		accessLog := models.AccessLog{
			AccessCodeId: accessCode.ID,
		}
		accessLog.Update();
		accessCode.Count ++;
		accessCode.Update();
		store.Set("cacheAccessCode", accessCode)
		h.JSON(w,Map{"status":1},http.StatusOK)
	}else{
		h.JSON(w,Map{"error":err.Error()},http.StatusInternalServerError)
	}
}

func (h authHandler) loginAccessCodePost(w http.ResponseWriter, r *http.Request) {
	store := sessionManager.Start(w, r)
	if access, ok := store.Get("cacheAccessCode").(models.AccessCode);ok{
		user := models.User{
			UserName: r.FormValue("UserName"),
			Password: r.FormValue("Password"),
		}
		if err := user.Create();err == nil {
			store.Delete("cacheAccessCode");
			access.Status = true;
			loginUserSession(w, r, user)
			h.JSON(w,Map{"status":1},http.StatusOK)
		}else{
			h.JSON(w,Map{"error":err.Error()},http.StatusInternalServerError)
		}
	}else {
		h.JSON(w,Map{"error":"Please back to login"},http.StatusBadRequest)
	}
}

func (h authHandler) confirmHandler(w http.ResponseWriter, r *http.Request) {
	store := sessionManager.Start(w, r)
	if user, ok := store.Get("cacheUser").(models.User);ok{
		if user.Password == r.FormValue("Password") {
			if err := user.Create();err == nil {
				store.Delete("cacheUser")
				loginUserSession(w, r, user)
				h.JSON(w,Map{"status":1},http.StatusOK)
			}else{
				h.JSON(w,Map{"error":err.Error()},http.StatusInternalServerError)
			}
		}else{
			h.JSON(w,Map{"error":"Password is not match"},http.StatusBadRequest)
		}
	}else {
		h.JSON(w,Map{"error":"Please back to login"},http.StatusBadRequest)
	}
}

func (h authHandler) logoutHandler(w http.ResponseWriter, r *http.Request) {
	store := sessionManager.Start(w, r)
	store.Delete("user")
	http.Redirect(w, r, "/auth/login", 302)
}