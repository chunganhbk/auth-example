package handlers

import (
	"encoding/json"
	"errors"
	"github.com/go-chi/chi"
	"github.com/kataras/go-sessions"
	"gitlab.com/chunganhbk/auth-example/helpers"
	"gitlab.com/chunganhbk/auth-example/models"
	"html/template"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"
)

type (
	Map map[string]interface{}
)

var (
	sessionManager *sessions.Sessions
	tmplHelper *helpers.TmplHelper
)

func InitHandlers(route *chi.Mux) {
	tmplHelper,_ = helpers.NewTPL(helpers.TmplConfig{"tpl","html","error_notFound"}, funcMap())
	sessionManager = sessions.New(sessions.Config{
		Cookie:  "demo-user",
		Expires: time.Hour * 24 * 365,
	})
	go createDbDemo();
	workDir, _ := os.Getwd()
	filesDir := filepath.Join(workDir, "public")
	FileServer(route, "/public", http.Dir(filesDir))
	route.Get("/", func(w http.ResponseWriter, r *http.Request) {
		store := sessionManager.Start(w, r)
		access := store.Get("cacheAccessCode");
		tmplHelper.Render(w,"demo_access",Map{"access": access})
	})

	route.Route("/auth",authRoute)
	route.Route("/admin",adminRoute)
	//route.Route("/user",userRoute)
	//route.Route("/cases",caseRoute)
}

func FileServer(r chi.Router, path string, root http.FileSystem) {
	if strings.ContainsAny(path, "{}*") {
		panic("FileServer does not permit URL parameters.")
	}

	fs := http.StripPrefix(path, http.FileServer(root))

	if path != "/" && path[len(path)-1] != '/' {
		r.Get(path, http.RedirectHandler(path+"/", 301).ServeHTTP)
		path += "/"
	}
	path += "*"

	r.Get(path, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fs.ServeHTTP(w, r)
	}))
}

func funcMap() template.FuncMap {
	return template.FuncMap{
		"json": func(data interface{}) string {
			_d,_ := json.Marshal(data)
			return string(_d)
		},
	}
}

type (
	handlerHelper struct {

	}
)

/*func (h handlerHelper) Render()  {

}*/

func (h handlerHelper) JSON(w http.ResponseWriter,data interface{},code int)  {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	body,_ := json.Marshal(data)
	w.Write(body)
}

func (h handlerHelper) ParseJSON(r *http.Request,data interface{}) error {
	if r.Body == nil {
		return errors.New("Body is nil")
	}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(data)
	defer r.Body.Close()
	return err
}

func createDbDemo()  {
	user := models.User{
		UserName: "admin",
		DisplayName: "Admin",
		Password: "123456",
		Role: 1,
	}
	user.Create()

}