package handlers

import (
	"gitlab.com/chunganhbk/auth-example/models"
	"net/http"
	"context"
	"errors"
	"time"
)

func needLoginMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		user,err := getUserSession(w,r)
		if err != nil {
			http.Redirect(w, r, "/auth/login", 302)
			return
		}
		ctx := context.WithValue(r.Context(), "user", user)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func loginUserSession(w http.ResponseWriter, r *http.Request, user models.User) {
	store := sessionManager.Start(w, r)
	user.LastLogin = time.Now()
	user.Update()
	store.Set("user", user)
}

func getUserSession(w http.ResponseWriter, r *http.Request) (user models.User, err error) {
	store := sessionManager.Start(w, r)
	var ok bool
	user, ok = store.Get("user").(models.User)
	if !ok {
		err = errors.New("Can not get user data")
	}
	return
}
