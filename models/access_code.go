package models

import (
	"fmt"
	"gitlab.com/chunganhbk/auth-example/database"
	"time"
)

type (
	AccessCode struct {
		ID              	int    `storm:"id,increment"`
		AccessCode          string `storm:"unique"`
		Status              bool
		Count               int
		LastModified        time.Time
		CreatedAt       	time.Time

	}
)
func (access *AccessCode) Create() error {
	if !database.Storage.Opened {
		return fmt.Errorf("db must be opened before deleting")
	}
	access.CreatedAt = time.Now()
	access.Status = false;
	access.Count = 0;
	err := database.Storage.DB.Save(access);
	return err;
}
func (u AccessCode) GetAll() (access []AccessCode, err error) {
	err = database.Storage.DB.All(&access)
	return
}

func (u AccessCode) GetList(f UserFilter) (access []AccessCode, err error) {

	query := database.Storage.DB.Select()
	if (f.SortKey != "") {
		query = query.OrderBy(f.SortKey)
		if f.SortVal < 0 {
			query = query.Reverse()
		}
	}
	err = query.Find(&access)
	return
}

func (access *AccessCode) Update() error {
	if !database.Storage.Opened {
		return fmt.Errorf("db must be opened before deleting")
	}

	access.LastModified = time.Now()
	return database.Storage.DB.Update(access)
}
func (access *AccessCode) Delete() error {
	if !database.Storage.Opened {
		return fmt.Errorf("db must be opened before deleting")
	}
	log := AccessLog{ AccessCodeId: access.ID};
	log.Delete();
	return database.Storage.DB.DeleteStruct(access);


}
func (u *AccessCode) GetByCode() error {
	return database.Storage.DB.One("AccessCode", u.AccessCode, u)
}