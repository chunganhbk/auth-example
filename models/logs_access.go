package models

import (
	"fmt"
	"github.com/asdine/storm/q"
	"gitlab.com/chunganhbk/auth-example/database"
	"time"
)

type (
	AccessLog struct {
		ID              	int    		`storm:"id,increment"`
		AccessCodeId				int
		Name				string
		Count               int
		CreatedAt			time.Time
		UpdatedAt 			time.Time
	}
)
func (c *AccessLog) CreateLog() (log AccessLog,err error) {
	c.Count = 1;
	c.CreatedAt = time.Now();
	c.UpdatedAt = time.Now();
	err = database.Storage.DB.Save(c);

	return
}

func (log  AccessLog) GetLogs(id int) (logs []AccessLog, err error) {
	err = database.Storage.DB.Select(q.Eq("AccessCodeId",id)).Find(&logs)
	return
}
func (c *AccessLog) Update() (log AccessLog,err error) {
	now := time.Now();
	currentLocation := now.Location()
	from := time.Date(now.Year(),now.Month(), now.Day(), 0, 0, 0, 0 , currentLocation);
	to := time.Date(now.Year(),now.Month(), now.Day(), 23, 59, 59, 99 , currentLocation);
	err = database.Storage.DB.Select(q.Gte("CreatedAt", from), q.Lte("CreatedAt", to), q.Eq("AccessCodeId", c.AccessCodeId)).First(c);
	fmt.Print(err);
	if err != nil{
		log,err = c.CreateLog();
	}else {
		c.Count ++;
		c.UpdatedAt = time.Now();
		err = database.Storage.DB.Update(c);
	}

	return
}

func (c *AccessLog) GetByID() error {
	return database.Storage.DB.One("ID", c.ID, c)
}
func (c *AccessLog) Delete() error {
	return database.Storage.DB.DeleteStruct(c)
}