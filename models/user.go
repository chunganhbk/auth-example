package models

import (
	"fmt"
	"github.com/asdine/storm/q"
	"gitlab.com/chunganhbk/auth-example/database"
	"golang.org/x/crypto/bcrypt"
	"time"
)
type (
	User struct {
		ID              	int    `storm:"id,increment"`
		UserName            string `storm:"unique"`
		DisplayName			string
		Password        	string
		CreatedAt       	time.Time
		LastLogin       	time.Time
		LastModified    	time.Time

		Admin				bool
		Role 				int
	}
	UserFilter struct {
		Sort
		Role				int
		UsersId 			[]int
	}
)
type (
	Sort struct {
		SortKey 	string
		SortVal		int
	}
)
func (user *User) Create() error {
	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.MinCost)
	if !database.Storage.Opened {
		return fmt.Errorf("db must be opened before deleting")
	}
	if err != nil {
		return err
	}
	user.Password = string(hash)
	user.CreatedAt = time.Now()
	user.LastModified = time.Now()
	err = database.Storage.DB.Save(user);
	return err;
}

func (u *User) Update() error {
	if !database.Storage.Opened {
		return fmt.Errorf("db must be opened before deleting")
	}
	u.LastModified = time.Now()
	return database.Storage.DB.Update(u)
}

func (u *User) UpdateField(name string,value interface{}) error {
	u.LastModified = time.Now()
	return database.Storage.DB.UpdateField(u,name,value)
}

func (u *User) Delete() error {
	return database.Storage.DB.DeleteStruct(u)
}

func (u User) GetAll() (users []User, err error) {
	err = database.Storage.DB.All(&users)
	return
}

func (u *User) GetByName() error {
	return database.Storage.DB.One("UserName", u.UserName, u)
}

func (u *User) GetByID() error {
	return database.Storage.DB.One("ID", u.ID, u)
}

func (u *User) ComparePassword(password string) error {
	return bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password))
}

func (u User) Display() string {
	if u.DisplayName != "" {
		return u.DisplayName
	}
	return u.UserName
}

func (u User) GetList(f UserFilter) (users []User, err error) {
	matchers := []q.Matcher{}
	switch f.Role {
	case 1:
	case 2:
		matchers = append(matchers,q.Eq("Role",f.Role))
	case 3:
		matchers = append(matchers,q.Eq("Role",0))
	}

	if len(f.UsersId) > 0 {
		matchers = append(matchers,q.In("ID",f.UsersId))
	}
	query := database.Storage.DB.Select(matchers...)
	if (f.SortKey != "") {
		query = query.OrderBy(f.SortKey)
		if f.SortVal < 0 {
			query = query.Reverse()
		}
	}
	err = query.Find(&users)
	return
}
