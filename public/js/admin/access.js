(function () {
    var app = {};
    Vue.component('v-sort',{
        template: `<span>
            <i v-if="sort == 1" class="event fas fa-caret-up text-dark" @click="change(-1)"></i>
            <i v-else-if="sort == -1" class="event fas fa-caret-down text-dark" @click="change(0)"></i>
            <i v-else class="event fas fa-sort" @click="change(1)"></i>
        </span> `,
        props: ['sort'],
        methods: {
            change: function (val) {
                //this.sort = val;
                this.$emit('sortChange',val)
            }
        }
    });
    Vue.component('access-e',{
        template: `<tr>
            <td>
              <input type="text" name="Value" v-if="editting" v-model="cacheVal.AccessCode" class="form-control" >
                <a   v-else :href="'/admin/access/'+item.ID">{{item.AccessCode}}</a>
                </td>
            <td>{{new Date(item.CreatedAt).view()}}</td>
            <td>{{item.Count }}</td>
           <td class="text-center">
                <div v-if="editting">
                    <a class="event text-success" @click="save">Save</a> | 
                    <a class="event text-danger" @click="editting = false">Cancel</a>
                </div>
                <div v-else>
                    <a class="event text-info" @click="edit(item)">Edit</a> | 
                    <a class="event text-danger" @click="del(item.ID)">Delete</a>
                </div>
            </td>
            <!--<td class="text-center">
                <div class="item-action dropdown">
                    <a href="javascript:void(0)" data-toggle="dropdown" class="icon"><i class="fe fe-more-vertical"></i></a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a :href="'/admin/users/'+user.ID" class="dropdown-item text-info"><i class="dropdown-icon fe fe-eye"></i> View </a>
                        <a :href="'/admin/users/worklog/'+user.ID" class="dropdown-item text-info"><i class="dropdown-icon fe fe-clipboard"></i> View Work Logs </a>
                        <a :href="'/admin/users/workclock/'+user.ID" class="dropdown-item text-info">
                            <i class="dropdown-icon fe fe-watch"></i> View Work Clocks 
                        </a>
                    </div>
                </div>
            </td>-->
        </tr>`,
        props: ['item'],
        data: function(){
            return {
                editting: false,
                cacheVal: {}
            }
        },
        methods: {
            localTime: function () {
                return Helper.showLocalTime(this.user.Timezone)
            },
            save: function () {
                const that = this;
                if(!this.cacheVal.AccessCode){
                    alert("Access code can't be blank");
                    return
                }
                $.ajax({
                    url: '/admin/access/update',
                    method: 'put',
                    data: this.cacheVal,
                    success: function (_d) {
                        that.editting = false;
                        app.getData();
                    },
                    error: function (err) {
                        toastr.error(xhr.responseJSON.error,'Edit fail!');
                    }
                })
            },
            edit: function (data) {
                this.cacheVal = {ID: data.ID, AccessCode: data.AccessCode};
                this.editting = true;
            },
            del: function (ID) {
                if (confirm("Are you sure you want to delete the option?")) {
                    $.ajax({
                        url:'/admin/access/'+ID,
                        method:'delete',
                        success: function () {
                            app.getData();
                        },
                        error: function () {
                            toastr.error(xhr.responseJSON.error,'Delete fail!')
                        }
                    })
                }
            }
        },
        computed: {
            role: function () {
                if(this.user.Role){
                    switch (this.user.Role) {
                        case 1: return 'Admin';
                        case 2: return 'Support Rep'
                    }
                }
                return 'Customer';
            }
        }
    });
    Vue.component('access-new',{
        template:`<tr>
            <td>
                <input type="text" class="form-control" name="Value" v-model="cacheVal.access_code">
            </td>
             <td></td>
            <td></td>
            <td class="text-center">
                <a class="event text-success" @click="save">Save</a> | 
                <a class="event text-danger" @click="cancel">Cancel</a>
            </td>
        </tr>`,
        methods: {
            save: function () {
                const that = this;
                if(!this.cacheVal.access_code){
                    alert("Name can't be blank");
                };
                $.ajax({
                    url: '/admin/access/create',
                    method: 'post',
                    data: that.cacheVal,
                    success: function (_d) {
                        app.getData();
                        that.$emit('close',true);
                    },
                    error: function (err) {
                        toastr.error(xhr.responseJSON.error,'Create fail!');
                    }
                })
            },
            cancel: function () {
                this.$emit('close',true);
            }
        },
        data: function () {
            return {
                cacheVal: {}
            }
        }
    });
    Vue.component('access-v',{
        template: `<div class="card">
            <div class="card-body">
             
                <div class="col-12"> 
                    <table class="table card-table table-striped table-vcenter">
                        <thead>
                        <tr>
                            <th>Access code</th>
                            <th>Created At <v-sort :sort="sort.CreatedAt" @sortChange="sortChange($event,'CreatedAt')"></v-sort></th>
                            <th>Visited</th>
                            <th>Logs</th>
                        </tr>
                        </thead>
                        <tbody>
                            <access-e v-for="u of lists" :item="u"></access-e>
                            <access-new v-if="creating" @close="creating = false"></access-new>
                        </tbody>
                         <tfoot>
                            <tr>
                                <td colspan="3">
                                    <button type="button" @click="creating = true" class="btn btn-secondary ml-auto">Add Access Code</button>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>`,
        props: ['lists'],
        data: function () {
            return {
                sort: {
                    AdminStatus: 0,
                    Status: 0,
                    CreatedAt: 0
                },
                disabled :false,
                creating: false
            }
        },
        methods: {
            sortChange: function (value,key) {
                if (this.sort[key] !== undefined) {
                    for (var i in this.sort) {
                        if (i == key) {
                            this.sort[i] = value;
                            this.$emit('sort',{
                                SortKey: i,
                                SortVal: value
                            });
                        }else{
                            this.sort[i] = 0;
                        }
                    }
                }
            }
        }
    });

    app = new Vue({
        el: '#access-cpt',
        template: '<access-v :lists="lists" @sort="sortChange($event)"></access-v>',
        data: function () {
            return {
                lists: [],
                filter: {},
                sort: {}
            }
        },
        methods: {
            getData: function (filter) {
                if(filter) {
                    this.filter = filter;
                }
                filter = Object.assign(this.filter,this.sort);
                const that = this;
                that.lists = [];
                $.ajax({
                    url: '/admin/access/list',
                    data: filter,
                    dataType : 'json',
                    success: function (_d) {
                        if(Array.isArray(_d) && _d.length) {
                            that.lists = _d;
                        }
                    },
                    error: function () {

                    }
                })
            },
            sortChange: function (newSort) {
                if (newSort.SortVal) {
                    this.sort = newSort;
                }else{
                    this.sort = {};
                }
                this.getData()
            }
        }
    });
    app.getData();
})();