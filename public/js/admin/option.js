(function () {
    var types = {
        Categories: 'Case Categories',
        ContactMethod: 'Contact Method',
        /*Currencies: 'Currencies',
        Languages: 'Languages'*/
    };
    window.app = {}
    Vue.component('option-elem',{
        template: `<tr>
            <td>
                <input type="text" name="Value" v-if="editting" v-model="cacheVal.Value" class="form-control">
                <span v-else>{{option.Value}}</span>
            </td>
            <td v-if="option.Type == 'Currencies'">
                <div v-if="editting">
                    <select name="IsDefault" class="form-control" v-model="cacheVal.IsDefault">
                        <option value="1">Yes</option>
                        <option value="2">No</option>
                    </select>
                </div>
                <div v-else v-html="isDefault"></div>
            </td>
            <td class="text-center">
                <div v-if="editting">
                    <a class="event text-success" @click="save">Save</a> | 
                    <a class="event text-danger" @click="editting = false">Cancel</a>
                </div>
                <div v-else>
                    <a class="event text-info" @click="edit">Edit</a> | 
                    <a class="event text-danger" @click="del">Delete</a>
                </div>
            </td>
        </tr>`,
        props: ['option'],
        data: function () {
            return {
                types: types,
                editting: false,
                cacheVal: {}
            }
        },
        computed: {
            isDefault: function (){
                return this.option.IsDefault == 1 ? '<i class="fe fe-check-circle text-success"></i>':'<i class="fe fe-minus-circle text-danger"></i>'
            }
        },
        methods: {
            save: function () {
                const that = this;
                if(!this.cacheVal.Value){
                    alert("Name can't be blank");
                    return
                }
                $.ajax({
                    url: '/admin/option/api',
                    method: 'put',
                    data: this.cacheVal,
                    success: function (_d) {
                        that.editting = false;
                        app.getData();
                    },
                    error: function (err) {
                        toastr.error(xhr.responseJSON.error,'Edit fail!');
                    }
                })
            },
            edit: function () {
                this.cacheVal = Object.assign({},this.option);
                this.editting = true;
            },
            del: function () {
                if (confirm("Are you sure you want to delete the option?")) {
                    let id = $(this).parent().data('id');
                    $.ajax({
                        url:'/admin/option/api/'+this.option.ID,
                        method:'delete',
                        success: function () {
                            app.getData();
                        },
                        error: function () {
                            toastr.error(xhr.responseJSON.error,'Delete fail!')
                        }
                    })
                }
            }
        }
    });
    Vue.component('option-new',{
        template:`<tr>
            <td>
                <input type="text" class="form-control" name="Value" v-model="cacheVal.Value">
            </td>
            <!--<td>
                <select name="IsDefault" class="form-control" v-model="cacheVal.IsDefault">
                    <option value="1">Yes</option>
                    <option value="2">No</option>
                </select>
            </td>-->
            <td class="text-center">
                <a class="event text-success" @click="save">Save</a> | 
                <a class="event text-danger" @click="cancel">Cancel</a>
            </td>
        </tr>`,
        methods: {
            save: function () {
                const that = this;
                if(!this.cacheVal.Value){
                    alert("Name can't be blank");
                }
                this.cacheVal.Type = app.target.value;
                $.ajax({
                    url: '/admin/option/api',
                    method: 'post',
                    data: that.cacheVal,
                    success: function (_d) {
                        app.getData();
                        that.$emit('close',true);
                    },
                    error: function (err) {
                        toastr.error(xhr.responseJSON.error,'Create fail!');
                    }
                })
            },
            cancel: function () {
                this.$emit('close',true);
            }
        },
        data: function () {
            return {
                cacheVal: {IsDefault:2}
            }
        }
    });
    Vue.component('options-cpt',{
        template: `
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{target.view}}</h3>
            </div>
            <div class="card-body">
                <div class="tab-content">
                    <div id="menu1" class="tab-pane fade in active show">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th v-if="target.value == 'Currencies'">Default</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                                <option-elem v-for="o of options" :option="o"></option-elem>
                                <option-new v-if="creating" @close="creating = false"></option-new>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="3">
                                    <button type="button" @click="creating = true" class="btn btn-secondary ml-auto">Add Option</button>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        `,
        props: ['options','target'],
        data: function () {
            return {
                creating: false
            }
        }
    });

    window.app = new Vue({
        el: '#options-ctn',
        data: function () {
            return {
                options: [],
                types: types,
                target: {
                    value: 'ContactMethod',
                    view: 'Contact Method'
                }
            }
        },
        methods: {
            getData: function () {
                const that = this;
                this.options = [];
                $.ajax({
                    url: "/admin/option/api",
                    data: {Type: that.target.value},
                    success: function (_d) {
                        if(Array.isArray(_d)){
                            that.options = _d;
                        }
                    },
                    error: function () {
                    }
                })
            },
            change: function (key) {
                if (this.types[key]) {
                    this.target = {
                        value: key,
                        view: this.types[key]
                    }
                    this.getData();
                }
            }
        }
    });
    app.getData();
    $('#listOption').on('click','.list-group-item',function () {
        if($(this).hasClass('active')){
            return
        }
        $('#listOption .list-group-item').removeClass('active');
        $(this).addClass('active');
        app.change($(this).data('type'));
    });
})();