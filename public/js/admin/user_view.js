(function () {
    $('#formCtrlUser').submit(function (e) {
        e.preventDefault();
        const data = {
            ID: viewUser.ID,
            Role: $('#formCtrlUser [name="Role"]').val()
        }
        $.ajax({
            url: '/admin/users/update',
            data: data,
            method: 'post',
            success: function (_d) {
                toastr.success('User information updated');
            },
            error: function (xhr) {
                toastr.error(xhr.responseJSON.error)
            }
        })
    })
})();