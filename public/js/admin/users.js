(function () {
    var app = {};
    Vue.component('v-sort',{
        template: `<span>
            <i v-if="sort == 1" class="event fas fa-caret-up text-dark" @click="change(-1)"></i>
            <i v-else-if="sort == -1" class="event fas fa-caret-down text-dark" @click="change(0)"></i>
            <i v-else class="event fas fa-sort" @click="change(1)"></i>
        </span> `,
        props: ['sort'],
        methods: {
            change: function (val) {
                //this.sort = val;
                this.$emit('sortChange',val)
            }
        }
    });
    Vue.component('user-e',{
        template: `<tr>
            <td><a :href="'/admin/users/'+user.ID">{{user.UserName}}</a></td>
            <td>{{new Date(user.CreatedAt).view()}}</td>
            <td>{{role}}</td>
            <!--<td class="text-center">
                <div class="item-action dropdown">
                    <a href="javascript:void(0)" data-toggle="dropdown" class="icon"><i class="fe fe-more-vertical"></i></a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a :href="'/admin/users/'+user.ID" class="dropdown-item text-info"><i class="dropdown-icon fe fe-eye"></i> View </a>
                        <a :href="'/admin/users/worklog/'+user.ID" class="dropdown-item text-info"><i class="dropdown-icon fe fe-clipboard"></i> View Work Logs </a>
                        <a :href="'/admin/users/workclock/'+user.ID" class="dropdown-item text-info">
                            <i class="dropdown-icon fe fe-watch"></i> View Work Clocks 
                        </a>
                    </div>
                </div>
            </td>-->
        </tr>`,
        props: ['user'],
        methods: {
            localTime: function () {
                return Helper.showLocalTime(this.user.Timezone)
            }
        },
        computed: {
            role: function () {
                if(this.user.Role){
                    switch (this.user.Role) {
                        case 1: return 'Admin';
                        case 2: return 'Support Rep'
                    }
                }
                return 'Customer';
            }
        }
    });
    Vue.component('users-v',{
        template: `<div class="card">
            <div class="card-body">
                <div class="col-12">
                    <table class="table card-table table-striped table-vcenter">
                        <thead>
                        <tr>
                            <th>User Name</th>
                            <th>Created At <v-sort :sort="sort.CreatedAt" @sortChange="sortChange($event,'CreatedAt')"></v-sort></th>
                            <th>Role</th>
                        </tr>
                        </thead>
                        <tbody>
                            <user-e v-for="u of users" :user="u"></user-e>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>`,
        props: ['users'],
        data: function () {
            return {
                sort: {
                    AdminStatus: 0,
                    Status: 0,
                    CreatedAt: 0
                }
            }
        },
        methods: {
            sortChange: function (value,key) {
                if (this.sort[key] !== undefined) {
                    for (var i in this.sort) {
                        if (i == key) {
                            this.sort[i] = value;
                            this.$emit('sort',{
                                SortKey: i,
                                SortVal: value
                            });
                        }else{
                            this.sort[i] = 0;
                        }
                    }
                }
            }
        }
    });

    app = new Vue({
        el: '#user-ctn',
        template: '<users-v :users="users" @sort="sortChange($event)"></users-v>',
        data: function () {
            return {
                users: [],
                filter: {},
                sort: {}
            }
        },
        methods: {
            getData: function (filter) {
                if(filter) {
                    this.filter = filter;
                }
                filter = Object.assign(this.filter,this.sort);
                const that = this;
                that.users = [];
                $.ajax({
                    url: '/admin/users/list',
                    data: filter,
                    dataType : 'json',
                    success: function (_d) {
                        if(Array.isArray(_d) && _d.length) {
                            that.users = _d;
                        }
                    },
                    error: function () {

                    }
                })
            },
            sortChange: function (newSort) {
                if (newSort.SortVal) {
                    this.sort = newSort;
                }else{
                    this.sort = {};
                }
                this.getData()
            }
        }
    });
    app.getData();
})();