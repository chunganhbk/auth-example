(function () {
    $('#accessCode').submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: '/auth/check-access-code',
            method: 'post',
            data: {
                access_code: $('input[name=access_code]').val(),
            },
            success: function (_d) {
                if (_d.status == 1) {
                    goNextAcess();
                }else {
                    toastr.error(xhr.responseJSON.error,"Login fail")
                }
            },
            error: function (xhr) {
                toastr.error("Access code has been used or not found","Login fail")
            }
        })
    });
    $('#loginForm').submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: '/auth/login-access-code',
            method: 'post',
            data: {
                UserName: $('input[name=UserName]').val(),
                Password: $('input[name=Password]').val(),
            },
            success: function (_d) {
                if (_d.status == 1) {
                    location.href = '/';
                }else {

                }
            },
            error: function (xhr) {
                toastr.error("Access code has been used or not found","Login fail")
            }
        })
    });
})();