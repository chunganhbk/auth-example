(function () {
    $('#loginForm').submit(function (e) {
        e.preventDefault();
        const data = {
            Username: $('#loginForm [name=Username]').val(),
            Password: $('#loginForm [name=Password]').val()
        };
        $.ajax({
            url: '/auth/login',
            method: 'post',
            data: data,
            success: function (_d) {
                location.href = '/';
            },
            error: function () {

            }
        });
        console.log(data)
    })
})();