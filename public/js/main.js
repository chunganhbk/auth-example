(function () {
    Date.prototype.view = function () {
        return `${this.getFullYear()}-${this.getMonth() + 1}-${this.getDate()}`
    };
    Date.prototype.viewHour = function () {
        let h = this.getHours();
        if(h > 12){
            h = h - 12;
            return `${h}:${this.getMinutes()}:${this.getSeconds()} PM`
        }
        return `${h}:${this.getMinutes()}:${this.getSeconds()}`
    };
    Date.prototype.viewFull = function () {
        return this.view() + ' ' + this.viewHour();
    };
    window.Helper = {
        showLocalTime : function (tz) {
            let t = new Date();
            if(typeof (tz) === 'string' && tz.indexOf("UTC") === 0){
                let tv = tz.slice(3);
                if (tv.length !== 6) {
                    return 'Unknown';
                }
                let fw = tv[0],sv = tv.slice(1),split = sv.split(":");
                let offset = parseInt(split[0]) + parseFloat(split[1]/60),
                    localOffset = (t.getTimezoneOffset() / -60);
                if(fw == "-"){
                    offset = - offset;
                }
                t.setTime(t.getTime() + (offset - localOffset) * 3600 * 1000);
                return t.viewHour();
            }
            return 'Unknown';
        },
        setUnreadContact: function (num) {
            if(num > 0){
                $('#unreadContactCount').text(num);
            }else{
                $('#unreadContactCount').text('');
            }
        }
    };


    /*--------------------------For message------------------------------*/
    /*window.ss = new SS('ws://'+window.location.host+'/ws');
    var contacts = [];
    ss.onConnect(function(){
        ss.emit("register",userId)
    });
    ss.onDisconnect(function(){
        console.log("Ws disconnected!")
    });
    function countUnread() {
        var count = 0;
        contacts.forEach(e => {
            if(!e.Viewed) {
                count += 1;
            }
        });
        console.log(count)
        Helper.setUnreadContact(count);
    }
    ss.on('contacts', function(c){
        if( Array.isArray(c)) {
            contacts = c;
        }
        countUnread();
    });
    ss.on("message",function (msg) {
        var c = contacts.find(e => {
            return e.ID == msg.ToId;
        });
        c.Viewed = false;
        if(c){
            countUnread();
        }
    });*/
    /*--------------------------End message------------------------------*/
})();
