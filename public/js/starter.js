(function () {
    $('#loginForm').submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: '/auth/login',
            method: 'post',
            data: {
                UserName: $('#loginForm [name=UserName]').val(),
                Password: $('#loginForm [name=Password]').val(),
            },
            success: function (_d) {
                if (_d.status == 1) {
                    location.href = '/admin';
                }else {
                    goNext();
                }
            },
            error: function (xhr) {
                toastr.error(xhr.responseJSON.error,"Login fail")
            }
        })
    });
    $('#repeatPassword').submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: '/auth/login/confirm',
            method: 'post',
            data: {
                Password: $('#repeatPassword [name=password]').val(),
            },
            success: function (_d) {
                location.href = '/user/profile';
            },
            error: function (xhr) {
                toastr.error(xhr.responseJSON.error,"Login fail")
            }
        })
    });
})();