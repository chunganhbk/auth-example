(function () {

    var formValid = {};
    $('#userSkill,#Languages').select2({
        tags: true,
        tokenSeparators: [',']
    });
    $('#userSkill,#Languages').on('change.select2',function () {
        formValid.element(this)
    });

    function checkToAddContact() {
        for(let i = 0;i < $('[name="ContactKey"]').length; i++) {
            if(!$(`[name="ContactKey"]:eq(${i})`).val()){
                return
            }
        }
        /*for(let i = 0;i < $('[name="Contact"]').length; i++) {
            if(!$(`[name="Contact"]:eq(${i})`).val()){
                return
            }
        }
        for(let i = 0;i < $('[name="ContactNew"]').length; i++) {
            if(!$(`[name="ContactNew"]:eq(${i})`).val()){
                return
            }
        }*/
        $('#contact-container').append($('#contact-content').html());
        $('#contact-container tr:last [name="ContactKey"]').val('');
        $('#contact-container tr:last [name="ContactKey"]').select2({
            tags: true,
            tokenSeparators: [',']
        });
    }

    $('#contact-container').on('blur','input',function () {
        checkToAddContact()
    });
    $(document).on('change','[name="ContactKey"]',function () {
        checkToAddContact();
    });
    checkToAddContact();
    $(document).on('click','.remove-contact',function () {
        $(this).parent().parent().remove();
        checkToAddContact()
    });
    formValid = $('#profileForm').validate({
        rules: {
            DisplayName: {required: true},
            /*Languages: {required: true},
            Timezone: {required: true},
            HoursPerWeek: {required: true,min:1},
            GoalHourlyRate: {required: true,min:1},
            ExpectedHourlyRate: {required: true,min:1},
            Skills: {required: true},*/
        },
        messages: {
            DisplayName: {required: 'Your name is required'},
            /*Languages: {required: 'Languages is required'},
            Timezone: {required: 'Timezone is required'},
            Skills: {required: 'Skills is required'},
            HoursPerWeek: {min: 'Please enter a value greater than zero'},
            GoalHourlyRate: {min: 'Please enter a value greater than zero'},
            ExpectedHourlyRate: {min: 'Please enter a value greater than zero'},*/
        },
        submitHandler: function (f) {
            var data = {
                DisplayName: $('#DisplayName').val(),
            },contact = {};
            if (!data.HourlyRate) {
                data.HourlyRate = 0;
            }
            for(var i = 0;i < $('[name="Contact"]').length;i++) {
                contact[$(`[name="Contact"]:eq(${i})`).data('key')] = $(`[name="Contact"]:eq(${i})`).val();
            }
            for(var i = 0;i < $('[name="ContactNew"]').length;i++) {
                let key = $(`[name="ContactNew"]:eq(${i})`).parent().parent().find('[name="ContactKey"]').val();
                if (key && !contact[key]) {
                    contact[key] = $(`[name="ContactNew"]:eq(${i})`).val();
                }
            }
            data.Contacts = contact;
            $.post('/user/profile',{
                data: JSON.stringify(data)
            },function (_d,status) {
                toastr.success(_d.message);
                if(_d.isFirst){
                    location.href = "/";
                }else{
                    location.href = "/";
                }
            }).fail(function (e) {
                toastr.error(e.responseJSON.error)
            })
        }
    });

})();